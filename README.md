# README #

This README is for customers for the Zambion platform who would like to create integrations through the Zambion API.

## What is this repository for? ##

### Quick summary ###

This repository stores the current Open API definition of the Zambion Customer API.

### Version ###

The current version of the Zambion Customer API is __0.1.0__.

## How do I get set up? ##

To get started, download the _zambion-customer-api.json_ file from the repository. You can then use the definition to create your own SDK using a code generator in the language of your choice, 
or import it into an Open API definition viewer, such as https://editor.swagger.io, for your reference.

__Please note that at this time, there is no active API endpoint to test your implementation against.__ However, you can use the Open API definition to generate your own stub implementation
while the project is in its early phases.

## Contribution guidelines ##

We are not currently accepting pull requests; but if you wish to propose changes to the API to support specific use cases or requirements, then you can fork the repository, and create your own 
branches to explore the changes to the Open API definition, which can then potentially be re-integrated into the Zambion Customer API.

## Who do I talk to? ##

* Repo owner or admin
* Other community or team contact